﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhyduino;
using System.Threading;

namespace Arduino_Firmatae
{
    class Examples
    {

        static void Main(string[] args)
        {
            Examples p = new Examples();

            // p.WriteEEPROM();

            p.test();
            
        }

        public Arduino arduino;
        public Firmatae firmatae;

        Examples()
        {
            //arduino = new Arduino("COM15", 115200, true);
            //arduino.debug = true;
        }

        //
        // This sets the EEPROM to a known string and reads it back
        // Max 31 chars
        //
        public void WriteEEPROM()
        {
            //arduino.EepromString = "ABCDEFG";
            Console.WriteLine( arduino.EepromString );
        }

        public void test()
        {
            List<string> d = new List<string>();
            d.Add("QWERTYUIOP");

            firmatae = new Firmatae(d, 115200);

            firmatae.Initialize();
        }


    }
}
