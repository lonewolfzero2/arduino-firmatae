﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using Rhyduino;
using System.Reflection;

namespace Arduino_Firmatae
{
    /*
     * Class to help map Arduino Firmata instances on any serial port based on their EEPROM IDs.
     * 
     * 
     */
    class Firmatae
    {

        // Serial port prefixes to filter out undesired ports
#if __MonoCS__
        // Linux
        string serialport_prefix = "/dev/ttyACM";
#else
        // Windows
        string serialport_prefix = "COM";
#endif

        // Defined IDs
        List<string> definitions;

        Dictionary<string, Arduino> arduinos = new Dictionary<string, Arduino>();
        bool Initialized = false;

        int default_baud;



        /*
         * Logging
         */
        public delegate void LogMessageDelegate(string msg);
        public LogMessageDelegate LogMessageHandler;

        protected void DefaultLogMessageHandler(string msg)
        {
            Console.WriteLine(msg);
        }
        




        /*
         * Create Firmatae with definition mapping Arduino Firmata IDs to objects with base type Arduino
         * 
         */
        public Firmatae(List<string> definition, int baud)
        {
            this.default_baud = baud;

            // Default logging handler
            LogMessageHandler = new LogMessageDelegate(DefaultLogMessageHandler);

            definitions = definition;
        }


        public void Initialize()
        {
            // ensure all IDs in definitions are mapped
            // List is small, we'll just iterate instead of using a map
            List<string> keys = new List<string>(definitions);

            Type[] constructor_args = new Type[3];
            constructor_args[0] = typeof(string);
            constructor_args[1] = typeof(int);
            constructor_args[2] = typeof(bool);
            
            foreach (string port in GetSerialPorts())
            {
                Arduino temparduino = null;
                string id = null;
                try
                {
                    temparduino = new Arduino(port, default_baud, true);
                    id = temparduino.EepromString;
                }
                catch (Exception e)
                {
                    LogMessageHandler("Error getting ID from " + port + ": " + e.Message);
                    if (temparduino != null)
                    {
                        try
                        {
                            temparduino.Disconnect();
                        }
                        catch (Exception)
                        {
                        }
                    }
                    // move on to next port
                    continue;
                }

                if (keys.Contains(id))
                {
                    arduinos[id] = temparduino;
                    keys.Remove(id);
                    // move on to next port
                    continue;
                }

                // not found, but not fatal, continue
                LogMessageHandler("Attached device of ID " + id + " on port " + port + " not in list");
            }


            // Check if any unmapped keys remaining
            if (keys.Count != 0)
            {
                string leftovers = null;
                foreach(string k in keys) {
                    if(leftovers == null)
                        leftovers = (string)k.Clone();
                    else
                        leftovers += ("," + k);
                }

                LogMessageHandler("Unable to map all defined Arduinos, left: " + leftovers);
                throw new Exception("Unable to map all defined Arduinos");
            }

            Initialized = true;
        }



        /*
         * Obtain mapped arduino object
         * 
         */
        public Arduino GetArduino(string arduino_id)
        {
            if (!Initialized) throw new Exception("Firmatae not initialized yet");
            return arduinos["arduino_id"];
        }






        /*
         * Scan and collect serial port names
         * - returns: List of serial port names
         */
        protected List<string> GetSerialPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            List<string> retports = new List<string>();

            foreach (string p in ports)
            {
                if (p.StartsWith(serialport_prefix))
                {
                    retports.Add(p);
                }
            }
            return retports;
        }



    }
}
