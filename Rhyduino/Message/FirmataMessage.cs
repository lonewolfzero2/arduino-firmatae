﻿//  Copyright © 2009-2010 by Rhy A. Mednick
//  All rights reserved.
//  http://rhyduino.codeplex.com
//  
//  Redistribution and use in source and binary forms, with or without modification, 
//  are permitted provided that the following conditions are met:
//  
//  * Redistributions of source code must retain the above copyright notice, this list 
//    of conditions and the following disclaimer.
//  
//  * Redistributions in binary form must reproduce the above copyright notice, this 
//    list of conditions and the following disclaimer in the documentation and/or other 
//    materials provided with the distribution.
//  
//  * Neither the name of Rhy A. Mednick nor the names of its contributors may be used 
//    to endorse or promote products derived from this software without specific prior 
//    written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System;

namespace Rhyduino.Message
{
    /// <summary>
    ///   Class containing the raw encoded data from a Firmata message.
    /// </summary>
    /// <remarks>
    ///   This class is immutable.
    /// </remarks>
    public class FirmataMessage
    {
        #region Constructor(s)

        /// <summary>
        ///   Creates and initializes a FirmataMessage object from an
        ///   encoded Firmata message. No decoding is performed.
        /// </summary>
        /// <param name = "message">The encoded message.</param>
        public FirmataMessage(byte[] message)
        {
            if (message == null) throw new ArgumentNullException("message");
            if (message.Length == 0) throw new ArgumentOutOfRangeException("message");
            _rawData = message;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Gets the raw (encoded) bytes contained in the message.
        /// </summary>
        /// <returns>The encoded message.</returns>
        public byte[] GetRawMessage()
        {
            return (byte[]) _rawData.Clone();
        }

        #endregion

        #region Private Fields

        private readonly byte[] _rawData;

        #endregion
    }
}