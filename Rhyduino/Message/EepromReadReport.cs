﻿//  Copyright © 2009-2010 by Rhy A. Mednick
//  All rights reserved.
//  http://rhyduino.codeplex.com
//  
//  Redistribution and use in source and binary forms, with or without modification, 
//  are permitted provided that the following conditions are met:
//  
//  * Redistributions of source code must retain the above copyright notice, this list 
//    of conditions and the following disclaimer.
//  
//  * Redistributions in binary form must reproduce the above copyright notice, this 
//    list of conditions and the following disclaimer in the documentation and/or other 
//    materials provided with the distribution.
//  
//  * Neither the name of Rhy A. Mednick nor the names of its contributors may be used 
//    to endorse or promote products derived from this software without specific prior 
//    written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Globalization;
using System.Text;

namespace Rhyduino.Message
{
    /// <summary>
    ///   Class containing the data contained in an encoded Firmata version report message.
    /// </summary>
    public class EepromReadReport : FirmataMessage
    {
        #region Propertie(s)

        /// <summary>
        ///   Eeprom data which was read
        /// </summary>
        public byte[] EepromData { get; private set; }

        #endregion

        #region Constructor(s)

        /// <summary>
        ///   Eeprom report which extracts EepromData from message
        /// </summary>
        public EepromReadReport(byte[] message) : base(message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            // check format of message
            if (message[0] != (byte) ResponseMessageType.SysexStart)
            {
                throw new FormatException(
                    String.Format(CultureInfo.CurrentCulture,
                                  "Message was not in expected format. Expected: message[0] = {0}",
                                  ResponseMessageType.SysexStart.ToString("X")));
            }
            if (message[1] != (byte) SysexType.READ_EEPROM_DATA)
            {
                throw new FormatException(
                    String.Format(CultureInfo.CurrentCulture,
                                  "Message was not in expected format. Expected: message[1] = {0}",
                                  SysexType.READ_EEPROM_DATA.ToString("X")));
            }
            if (message[message.GetUpperBound(0)] != (byte) ResponseMessageType.SysexEnd)
            {
                throw new FormatException(
                    String.Format(CultureInfo.CurrentCulture,
                                  "Message was not in expected format. Expected: message[{0}] = {1}",
                                  message.GetLowerBound(0), ResponseMessageType.ProtocolVersion.ToString("X")));
            }

            // now pull out the data
            int datalen = reconstructByte( message[2], message[3] );

            if (((message.Length - 5) / 2) != datalen)
            {
                throw new FormatException("datalen check error");
            }

            byte[] data = new byte[datalen];
            for (int i = 0; i < datalen; ++i)
            {
                data[i] = reconstructByte(message[4 + (i * 2)], message[5 + (i * 2)]);
            }
            
            EepromData = data;
        }

        /// <summary>
        ///   Helper function to reconstruct 2-byte message into single bytes
        /// </summary>
        protected byte reconstructByte(byte lsb, byte msb)
        {
            lsb &= 0x7F;
            msb &= 0x7F;
            return (byte)(lsb | (msb << 7));
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Returns a string describing the object.
        /// </summary>
        /// <returns>The string</returns>
        public override string ToString()
        {
            return System.Text.Encoding.UTF8.GetString(EepromData);
        }

        #endregion
    }
}