﻿//  Copyright © 2009-2010 by Rhy A. Mednick
//  All rights reserved.
//  http://rhyduino.codeplex.com
//  
//  Redistribution and use in source and binary forms, with or without modification, 
//  are permitted provided that the following conditions are met:
//  
//  * Redistributions of source code must retain the above copyright notice, this list 
//    of conditions and the following disclaimer.
//  
//  * Redistributions in binary form must reproduce the above copyright notice, this 
//    list of conditions and the following disclaimer in the documentation and/or other 
//    materials provided with the distribution.
//  
//  * Neither the name of Rhy A. Mednick nor the names of its contributors may be used 
//    to endorse or promote products derived from this software without specific prior 
//    written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
namespace Rhyduino
{
    using System;
    using System.Globalization;

    /// <summary>
    ///   Class used internally by the application as part of the serial port 
    ///   auto-detection algorithm. It might be removed prior to release.
    /// </summary>
    internal class PortReport : EventArgs
    {
        #region Propertie(s)

        public string PortName { get; private set; }
        public int FirmataMajorVersion { get; private set; }
        public int FirmataMinorVersion { get; private set; }
        public string FirmataName { get; private set; }
        public bool Invalid { get; private set; }

        #endregion

        #region Constructor(s)

        internal PortReport(string portName, int majorVersion, int minorVersion, string firmataName)
        {
            PortName = portName;
            FirmataMajorVersion = majorVersion;
            FirmataMinorVersion = minorVersion;
            FirmataName = firmataName;
            Invalid = false;
        }

        internal PortReport(string portName)
        {
            PortName = portName;
            Invalid = true;
        }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return Invalid
                       ? String.Format(CultureInfo.CurrentCulture, "[{0}] Non-Arduino Port", PortName)
                       : String.Format(CultureInfo.CurrentCulture, "[{0}] {1}.{2} {3}", PortName, FirmataMajorVersion,
                                       FirmataMinorVersion, FirmataName);
        }

        #endregion
    }
}
