﻿//  Copyright © 2009-2010 by Rhy A. Mednick
//  All rights reserved.
//  http://rhyduino.codeplex.com
//  
//  Redistribution and use in source and binary forms, with or without modification, 
//  are permitted provided that the following conditions are met:
//  
//  * Redistributions of source code must retain the above copyright notice, this list 
//    of conditions and the following disclaimer.
//  
//  * Redistributions in binary form must reproduce the above copyright notice, this 
//    list of conditions and the following disclaimer in the documentation and/or other 
//    materials provided with the distribution.
//  
//  * Neither the name of Rhy A. Mednick nor the names of its contributors may be used 
//    to endorse or promote products derived from this software without specific prior 
//    written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System;

namespace Rhyduino
{
    ///<summary>
    ///  Class for managing the state of the analog pins.
    ///</summary>
    public class AnalogPins
    {
        #region Constructor(s)

        internal AnalogPins(Arduino arduino)
        {
            _analogPins = new[]
                              {
                                  new AnalogPin(arduino, 0),
                                  new AnalogPin(arduino, 1),
                                  new AnalogPin(arduino, 2),
                                  new AnalogPin(arduino, 3),
                                  new AnalogPin(arduino, 4),
                                  new AnalogPin(arduino, 5)
                              };
        }

        #endregion

        #region Propertie(s)

        ///<summary>
        ///  Gets the pin information for the specified analog pin.
        ///</summary>
        ///<param name = "pinNumber">The analog pin number.</param>
        ///<exception cref = "InvalidOperationException">When the value specified for 
        ///  the pinNumber is not a valid pin number.</exception>
        public AnalogPin this[int pinNumber]
        {
            get { return _analogPins[pinNumber]; }
        }

        #endregion

        #region Fields

        private readonly AnalogPin[] _analogPins;

        #endregion

        #region Private Methods

        #endregion
    }
}