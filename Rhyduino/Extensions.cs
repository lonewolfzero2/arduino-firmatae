﻿//  Copyright © 2009-2010 by Rhy A. Mednick
//  All rights reserved.
//  http://rhyduino.codeplex.com
//  
//  Redistribution and use in source and binary forms, with or without modification, 
//  are permitted provided that the following conditions are met:
//  
//  * Redistributions of source code must retain the above copyright notice, this list 
//    of conditions and the following disclaimer.
//  
//  * Redistributions in binary form must reproduce the above copyright notice, this 
//    list of conditions and the following disclaimer in the documentation and/or other 
//    materials provided with the distribution.
//  
//  * Neither the name of Rhy A. Mednick nor the names of its contributors may be used 
//    to endorse or promote products derived from this software without specific prior 
//    written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Rhyduino
{
    /// <summary>
    ///   Class containing the extension methods used by the application.
    /// </summary>
    public static class Extensions
    {
        #region Public Methods

        /// <summary>
        ///   Formats a byte array into an easily readable string for debugging purposes.
        /// </summary>
        /// <param name = "value">The byte array to format.</param>
        /// <returns>The formatted string</returns>
        public static string ToHexString(this byte[] value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            var sb = new StringBuilder();
            sb.AppendFormat("Byte[{0}]{1}", value.Length, "{");
            for (var index = 0; index < value.Length; index++)
            {
                var b = value[index];
                // format the byte as a two-digit hex string.
                sb.Append(b.ToString("X2", CultureInfo.CurrentCulture));
                if (index + 1 != value.Length)
                {
                    sb.Append(" ");
                }
            }
            sb.Append("}");
            return sb.ToString();
        }

        /// <summary>
        ///   Takes an integer value and formats it as two 7-bit bytes
        ///   for sending as part of a Firmata message.
        /// </summary>
        /// <param name = "value">The value to convert.</param>
        /// <returns>The formatted bytes.</returns>
        public static byte[] ToTwo7BitBytes(this int value)
        {
            return new[]
                       {
                           (byte) (value & 0x7F),
                           (byte) ((value >> 7) & 0x7F)
                       };
        }

        /// <summary>
        ///   Removes trailing null bytes from
        /// </summary>
        /// <param name = "value">The byte array to trim.</param>
        /// <returns>A new array with the trailing null bytes removed.</returns>
        public static byte[] Trim(this byte[] value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            var top = value.Length;
            // this just decrements until it finds something that's not null
            while ((top > 0) && (value[--top] == 0))
            {
            }

            return value.Where((x, y) => y < top + 1).ToArray();
        }

        #endregion
    }
}