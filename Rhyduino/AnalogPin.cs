﻿//  Copyright © 2009-2010 by Rhy A. Mednick
//  All rights reserved.
//  http://rhyduino.codeplex.com
//  
//  Redistribution and use in source and binary forms, with or without modification, 
//  are permitted provided that the following conditions are met:
//  
//  * Redistributions of source code must retain the above copyright notice, this list 
//    of conditions and the following disclaimer.
//  
//  * Redistributions in binary form must reproduce the above copyright notice, this 
//    list of conditions and the following disclaimer in the documentation and/or other 
//    materials provided with the distribution.
//  
//  * Neither the name of Rhy A. Mednick nor the names of its contributors may be used 
//    to endorse or promote products derived from this software without specific prior 
//    written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Rhyduino
{
    ///<summary>
    ///  Class used to evaluate the values received from an analog pin.
    ///</summary>
    ///<remarks>
    ///  This class can only be instantiated by another object in the Rhyduino library; you cannot create an 
    ///  instance of it on your own, and you cannot create derived classes from it.
    ///</remarks>
    public sealed class AnalogPin
    {
        #region Propertie(s)

        /// <summary>
        ///   Gets or sets the monitoring state of the pin. If true, the value of the pin will 
        ///   be updated approximately 50 times a second.
        /// </summary>
        public bool Monitor
        {
            get { return _monitor; }
            set
            {
                // If the value is changing notify the Arduino.
                if (_monitor != value)
                {
                    _arduino.Post(FirmataEncoder.BuildReportAnalogPinRequest(_pinNumber, value));
                }
                _monitor = value;
            }
        }


        ///<summary>
        ///  Gets or sets the number of previous pin values to keep a record of. 
        ///  Changing this value clears the cache.
        ///</summary>
        public int CacheSize
        {
            get { return _cacheSize; }
            set
            {
                _cacheSize = value;
                Interlocked.Exchange(ref _cache, new List<int>(_cacheSize));
            }
        }

        ///<summary>
        ///  The most recently reported value for the analog pin.
        ///</summary>
        public int Value
        {
            get { return _newestValue; }
            internal set
            {
                _newestValue = value;
                // Maintain the cache.
                if (_cache.Count >= _cacheSize)
                {
                    _cache.RemoveAt(0);
                }
                _cache.Add(_newestValue);
            }
        }

        /// <summary>
        ///   Gets or sets the number of values to use as the base for calculating the smoothed value.
        /// </summary>
        public int SmoothingRange { get; set; }

        /// <summary>
        ///   Gets the smoothed value of the pin. The smoothed value is the average of the values in the smoothing range.
        /// </summary>
        /// <remarks>
        ///   To give an example - If the SmoothingRange property were 10, then the SmoothedValue property
        ///   would be the average of the last 10 changes. This method for determining the value is slower to respond 
        ///   to the input, but is less noisy.
        /// </remarks>
        public int SmoothedValue
        {
            get
            {
                var max = (_cache.Count < SmoothingRange) ? _cache.Count : SmoothingRange;
                var smoothSet = new int[max];
                for (var i = 0; i < max; i++)
                {
                    smoothSet[i] = _cache[_cache.Count - (i + 1)];
                }
                return (int) smoothSet.Average();
            }
        }

        ///<summary>
        ///  Gets the cached pin values. The values are listed in the order they were received.
        ///</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public IEnumerable<int> GetCachedValues()
        {
            return _cache.ToArray();
        }

        #endregion

        #region Constructor(s)

        ///<summary>
        ///  Constructs an instance of the class.
        ///</summary>
        ///<param name = "arduino">The connected Arduino.</param>
        ///<param name = "pinNumber">The analog pin number.</param>
        ///<param name = "cacheSize">The number of previous values to store. When a pin is being monitored, it will 
        ///  receive new values at a rate of roughly 50 per second.</param>
        ///<param name = "initialValues"></param>
        internal AnalogPin(Arduino arduino, int pinNumber, int cacheSize = 500, ICollection<int> initialValues = null)
        {
            _cacheSize = cacheSize;
            _cache = new List<int>(cacheSize);
            _arduino = arduino;
            _pinNumber = pinNumber;

            if (initialValues == null) return;

            if (cacheSize < initialValues.Count)
            {
                // the list is too long, so we have to do this song and dance to get the 
                // newest items (at the end of the list).
                var subset = new int[_cacheSize];
                Array.Copy(initialValues.Reverse().ToArray(), subset, cacheSize);
                _cache.AddRange(subset.Reverse());
            }
            else
            {
                _cache.AddRange(initialValues);
            }

            _newestValue = (_cache.Count > 0) ? _cache.Last() : 0;
        }

        #endregion

        #region Private Fields

        private readonly Arduino _arduino;
        private readonly int _pinNumber;
        private List<int> _cache;
        private int _cacheSize;
        private bool _monitor;
        private int _newestValue;

        #endregion
    }
}